#!/usr/bin/python
#Autor: Ats Aim
#Versioon: 1.00
#Kirjeldus:Script loeb failist andmed (URL + String) ning otsib seejärel antud stringi URL saidilt ning kannab info output faili.
#Input failis peab URL ning string olema eraldatud tabiga ning iga URL ja string peab olema eraldi real!
#Kasutamine: parser.py /path/to/inputFile /path/to/outputFile

#Impordin vajalikud library'd.
import requests # (pip install requests) urli päringu jaoks
import sys	#argumentide edastamise jaoks scriptile

#Muutujate kontroll
if len(sys.argv) == 3:
	#Loon muutujad ning väärtustan inputi ja resulti faili path'id 
	input_file_path = sys.argv[1]
	result_file_path = sys.argv[2]
	file_content = None
	
	# Avan input faili ning loen selle sisu
	try:
		with open(input_file_path) as f:
			file_content = f.read()
	except IOError as e:
		print('File or Directory does not exist!')
		sys.exit(1)


	result = []
	#Splitin faili sisu vastavalt reavahetustele (\n) ning eejärel käin selle for loopiga läbi
	for line in file_content.split("\n"):
		#Loopis teen line'le spliti ning seda tabi korral (\t) see annab mulle kaks poolt iga line korral. Ehk URL ja Tekst
		#Lisan mõlemad muutujatesse
		url, text_to_find = line.split('\t')
		try:
			r = requests.get(url)
		except requests.exceptions.RequestException as e:    # This is the correct syntax
			print ("Critical Error with Link!/Request Exception!")
			continue
		except requests.exceptions.TooManyRedirects:
			print("Broken Link/Too many Redirects!")
			continue
		#Kontrollin kas antud string sisaldub URLi HTML-is. Vastavalt prindin ka vastuse. Lisan vajaliku info [URL, String, Vastus(JAH,EI)] result massiivi.
		if text_to_find in r.text:
			result.append((url, text_to_find, "JAH"))
			print('Text from ' + url + ' contains ' + text_to_find)
		else:
			result.append((url, text_to_find, "EI"))
			print('Text from ' + url + ' does not contain ' + text_to_find)
	#Avan result faili ning for loopiga käin läbi result massiivi, lisan result faili vajalikud sissekanded.
		with open(result_file_path, 'w+') as f:
			for r in result:
				f.write(r[0] + '\t' + r[1] + '\t' + r[2] + '\n')	
else:
	print("paths to input and output files must be given!")
