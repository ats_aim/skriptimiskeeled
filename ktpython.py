#!/usr/bin/python
#Autor: Ats Aim

import sys	#argumentide edastamise jaoks scriptile
import csv #CSV faili library

sisend = sys.argv[1]
valjund = sys.argv[2]
content = None

with open(sisend) as f:
	content = f.read()
	
result = []
for line in content.split("\n"):
	nr, nimi_grupp  = line.split('\t')
	if nr.isdigit():
		eesnimi, perenimi, grupp=nimi_grupp.split(' ')
		email=eesnimi+('.')+perenimi+('@itcollege.ee')
		kasutaja=eesnimi[0]+perenimi
		nimi=eesnimi+('')+perenimi
		random='asda1245Af'
		token=grupp+random
		result.append((kasutaja, nimi, email, token))
		with open(valjund, 'w+') as f:
			for r in result:
				f.write(r[0] + ',' + r[1] + ',' + r[2] + ',' + r[3] + '\n')