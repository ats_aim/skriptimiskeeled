#!/usr/bin/python
#Autor: Ats Aim
#Versioon: 1.00
import sys	#argumentide edastamise jaoks scriptile
import requests # (pip install requests) urli päringu jaoks
import datetime

if len(sys.argv) != 3:
	print("Use  arvestus.py URL filepath")	
	sys.exit(1)
	
URL = sys.argv[1]
sisend = sys.argv[2]
file_content = None

try:
	with open(sisend) as f:
		file_content = f.read()
except IOError as e:
	print('File or Directory does not exist!')
	sys.exit(1)

for line in file_content.split("\n"):
	path, textString = line.split(',')
	fullURL='http://'+URL+path
	dateTime=datetime.datetime.now()
	aeg = dateTime.strftime('%Y-%m-%d_%H-%M-%S')
	try:
		r = requests.get(fullURL)
	except requests.exceptions.RequestException as e:
		print ("Critical Error with Link!/Request Exception!")
		continue
	except requests.exceptions.TooManyRedirects:
		print("Broken Link/Too many Redirects!")
		continue
	if textString in r.text:
		print(fullURL + ',' + textString + ',' + aeg + ',' + 'OK')
	else:
		print(fullURL + ',' + textString + ',' + aeg + ',' 'NOK')	
