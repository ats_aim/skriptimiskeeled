#!/bin/bash
#Autor: Ats Aim
#Script väljastab kõoik võoimalikud pangakaartide PIN-koodid

export LC_ALL=C

#Juurkasutaja õiguste kontroll
if [ $UID -ne 0 ]
then
    echo "Root permission required!"
exit 1
fi

#Muutuja arvu kontroll
if [ $# -eq 1 ]
then
    PIN=$1
else
#For tsükkel mis väljastab kõik võimalikud panga PIN-KOODID kui ei ole muutujat
    for i in {0000..9999}; do 
        echo $i; 
    done
exit 1
fi
 
#For tsükliga käain läabi kõik vahemikus arvud ja kontrollin if lausega, kas muutuja on võrdne.   
for i in {0000..9999}; do  
    if [ $PIN -eq $i ]
    then
#Kui muutuja on võrdne I-ga siis teen kaustad ja failid ning jätkan
       echo "Loon manti!"
       test -d /home/student/Documents/$PIN || mkdir -p /home/student/Documents/$PIN
test -f /home/student/Documents/$PIN/koodid.txt && touch /home/student/Documents/$PIN/koodid$i.txt || touch /home/student/Documents/$PIN/koodid.txt 
       for j in $( seq -w $PIN 9999 ); do 
           echo $j >>/home/student/Documents/$PIN/koodid.txt
       done    
       sleep 3 
     else
            echo "$i";  
     fi
done