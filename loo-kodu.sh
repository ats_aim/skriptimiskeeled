#!/bin/bash
#Autor: Ats Aim
#Script loob uue veebikodu

export LC_ALL=C


#Juurkasutaja õiguste kontroll
if [ $UID -ne 0 ]
then
    echo "Root permission required!"
exit 1
fi


#Muutuja arvu kontroll
if [ $# -eq 1 ]
then
    AADRESS=$1
else
    echo "Use Script: $(basename $0) www.entername.ee"
exit 1
fi


#Veebikodu olemasolu kontroll hosts'is
grep $AADRESS /etc/hosts > /dev/null 2>&1 && echo "Address already exists! Please try another name!" && exit 1


#Apache paigalduse kontroll(Vajadusel paigaldamine)
type apache2 > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo "Apache is not installed.Installing!."
    apt-get update > /dev/null 2>&1 && apt-get install apache2 -y > /dev/null 2>&1 || exit 1
fi


#Nimelahenduse sissekanne /etc/hosts'i
echo "127.0.0.1 $AADRESS" >> /etc/hosts


#Confifailist koopia
echo "Making a copy of the configuration file."
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$AADRESS.conf


#Confifaili muutmine
sed -i "s-#ServerName www.example.com-ServerName $AADRESS-g" /etc/apache2/sites-available/$AADRESS.conf
sed -i "s-/var/www/html-/var/www/$AADRESS-g" /etc/apache2/sites-available/$AADRESS.conf


#Loon veevikodule füüsilise kataloogi
echo "Creating folder: /var/www/$AADRESS"
mkdir -p /var/www/$AADRESS


#Kopeerin default index.html faili
echo "Lisan vaikimisi index faili."
cp /var/www/html/index.html /var/www/$AADRESS/index.html


#Index faili muutmine
echo "Changing HTML"
echo "<html><head><title>$AADRESS</title></head><body><h1>$AADRESS</h1></body></html>" > /var/www/$AADRESS/index.html


#Luban virtuaalserveri kasutuse
echo "Enable Site"
a2ensite $AADRESS


#Reload apachele
echo "Reload Apache."
service apache2 reload > /dev/null
exit 0